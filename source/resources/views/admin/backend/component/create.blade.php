@extends('/admin/backend/base')

@section('content')
    <div class="row">
        <form class="col-md-4" method="POST" action="{{route('component.create')}}" enctype="multipart/form-data">
            @csrf
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Component Code</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" placeholder="Name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select name="type" class="form-control" style="text-transform: capitalize;">
                            @foreach($data['types'] as $type)
                            <option value="{{$type->id}}" style="text-transform: capitalize;">{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="component">HTML Component</label>
                        <input name="component" type="file" class="form-control-file">
                    </div>
                    <div class="form-group">
                        <label for="js">Javascript</label>
                        <input name="js" type="file" class="form-control-file">
                    </div>
                    <div class="form-group">
                        <label for="css">CSS</label>
                        <input name="css" type="file" class="form-control-file">
                    </div>
                    <button class="btn btn-primary btn-user btn-block" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Preview</h6>
                </div>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
@endsection
