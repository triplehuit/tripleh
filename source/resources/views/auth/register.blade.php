@extends('auth.app')

@section('content')
    <div class="login-logo">
        <a href="{{route('tenant.index')}}">
            <img class="align-content" src="{{asset('assets/template/images/logo.png')}}" alt="Logo">
        </a>
    </div>
    <div class="login-form">
        <form method="POST" action="{{url('register')}}">
            @csrf
            <div class="form-group">
                <input type="text" class="form-control" required data-msg="Please enter your username" class=" @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name">
                <label for="name" class="">Name</label>
                @error('name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="email" type="email" class=" form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                <label for="email" class="">Email Address</label>
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password" class=" form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                <label for="password" class="">Password</label>
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password-confirm" type="password" class=" form-control" name="password_confirmation" required autocomplete="new-password">
                <label for="password_confirm" class="">Confirm Password</label>
            </div>
            <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Register</button>
            <div class="social-login-content">
                <div class="social-button">
                    <button type="button" class="btn social facebook btn-flat btn-addon mb-3"><i class="ti-facebook"></i>Register with facebook</button>
                    <button type="button" class="btn social twitter btn-flat btn-addon mt-2"><i class="ti-twitter"></i>Register with twitter</button>
                </div>
            </div>
            <div class="register-link m-t-15 text-center">
                <p>Already have account ? <a href="{{route('login')}}"> Sign in</a></p>
            </div>
        </form>
    </div>
@endsection
