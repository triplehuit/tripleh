@extends('auth.app')

@section('content')
    <div class="login-logo">
        <a href="{{route('tenant.index')}}">
            <img class="align-content" src="{{asset('assets/template/images/logo.png')}}" alt="Logo">
        </a>
    </div>
    <div class="login-form">
        <form method="POST">
            @csrf
            <div class="form-group">
                <input id="email" type="email" name="email" required data-msg="Please enter your email" class="input-material form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email">
                <label for="email" class="label-material">Email</label>
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password" class="input-material form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" data-msg="Please enter your password">
                <label for="password" class="label-material">Password</label>
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember Me
                </label>
                @if (Route::has('password.request'))
                    <label class="pull-right">
                        <a  href="{{ route('password.request') }}">Forgotten Password?</a>
                    </label>
                @endif


            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
            <div class="social-login-content">
                <div class="social-button">
                    <button type="button" class="btn social facebook btn-flat btn-addon mb-3"><i class="ti-facebook"></i>Sign in with facebook</button>
                    <button type="button" class="btn social twitter btn-flat btn-addon mt-2"><i class="ti-twitter"></i>Sign in with twitter</button>
                </div>
            </div>
            <div class="register-link m-t-15 text-center">
                <p>Don't have account ? <a  href="{{route('register')}}"> Sign Up Here</a></p>
            </div>
        </form>
    </div>
@endsection
