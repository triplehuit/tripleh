@include('/tenant/frontend/baseNavbar')
@include('/tenant/frontend/baseFooter')

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>ShopMax</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="{{asset('/assets/shopmax/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/owl.theme.default.min.css')}}">


    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/shopmax/css/style.css')}}">

  </head>
  <body>
    <div class="site-wrap">
        @yield('navbar')
        @yield('content')
        @yield('footer')
    </div>

    <script src="{{asset('/assets/shopmax/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/jquery-ui.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/popper.min.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/aos.js')}}"></script>
    <script src="{{asset('/assets/shopmax/js/main.js')}}"></script>
  </body>
