@section('popular')
    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="title-section mb-5 col-12">
                    <h2 class="text-uppercase">Popular Products</h2>
                </div>
            </div>
            @php
                $popular_items = $data['popular_items'];
            @endphp
            <div class="row">
                @foreach($popular_items as $item)
                    <div class="col-lg-4 col-md-6 item-entry mb-4">
                        <a href="{{route('page.shopSingle', ['slug' => $slug, 'product_slug' => $item->slug])}}" class="product-item md-height bg-gray d-block">
                            <img src="{{asset('storage/'.$item->images()->first()->path)}}" alt="Image" class="img-fluid">
                        </a>
                        <h2 class="item-title"><a href="#">{{$item->name}}</a></h2>
                        <strong class="item-price">{{$item->price}}</strong>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
