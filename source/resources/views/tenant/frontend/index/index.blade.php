@extends('/tenant/frontend/base')
@include('/tenant/frontend/baseLanding')
@include('/tenant/frontend/baseCollection')
@include('/tenant/frontend/basePopular')
@include('/tenant/frontend/baseRated')
@include('/tenant/frontend/baseCover')

@section('content')
    @yield('landing')
    @yield('collection')
    @yield('popular')
{{--    @yield('rated')--}}
    @yield('cover')
@endsection
