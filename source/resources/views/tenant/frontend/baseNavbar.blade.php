@section('navbar')
<div class="site-navbar bg-white py-2">
    <div class="search-wrap">
        <div class="container">
            <a href="#" class="search-close js-search-close"><span class="icon-close2"></span></a>
            <form action="#" method="post">
                <input type="text" class="form-control" placeholder="Search keyword and hit enter...">
            </form>
        </div>
    </div>

    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <div class="logo">
                <div class="site-logo">
                    <a href="{{route('page.index', $tenant->slug)}}" class="js-logo-clone">{{$tenant->name}}</a>
                </div>
            </div>
            <div class="main-nav d-none d-lg-block">
                <nav class="site-navigation text-right text-md-center" role="navigation">
                    <ul class="site-menu js-clone-nav d-none d-lg-block">
                        <li class="has-children active">
                            <a href="{{route('page.index', $tenant->slug)}}">Home</a>
                            <ul class="dropdown">
                                @foreach($tenant->categories()->doesnthave('parents')->get() as $category)
                                    @if ($category->children()->count() > 0)
                                        <li class="has-children">
                                            <a href="{{route('page.category', ['slug' => $tenant->slug, 'id' => $category->id])}}">{{$category->name}}</a>
                                            <ul class="dropdown">
                                                @foreach($category->children()->get() as $childCategory)
                                                <li><a href="{{route('page.category', ['slug'=>$tenant->slug, 'id' => $childCategory->id])}}">{{$childCategory->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="{{route('page.category', ['slug' => $tenant->slug, 'id' => $category->id])}}">{{$category->name}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>

                        <li><a href="{{Route('page.shop', $tenant->slug)}}">Shop</a></li>
                        <li><a href="#">Catalogue</a></li>
                        <li><a href="#">New Arrivals</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="icons">
                <a href="#" class="icons-btn d-inline-block js-search-open"><span class="icon-search"></span></a>
                <a href="#" class="icons-btn d-inline-block"><span class="icon-heart-o"></span></a>
                <a href="cart.html" class="icons-btn d-inline-block bag">
                    <span class="icon-shopping-bag"></span>
                    <span class="number">2</span>
                </a>
                <a href="#" class="site-menu-toggle js-menu-toggle ml-3 d-inline-block d-lg-none"><span class="icon-menu"></span></a>
            </div>
        </div>
    </div>
</div>
@endsection
