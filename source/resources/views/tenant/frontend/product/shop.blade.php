@extends('/tenant/frontend/base')
@include('/tenant/frontend/baseLanding')
@include('/tenant/frontend/baseCollection')
@include('/tenant/frontend/baseShopAll')
@include('/tenant/frontend/baseBreakcumb')

@section('content')
    @yield('landing')
    @yield('breakcumb')
    @yield('shopAll')
    @yield('collection')
@endsection
