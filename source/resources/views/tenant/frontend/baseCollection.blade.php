@section('collection')
    <div class="site-section">
        <div class="container">
            <div class="title-section mb-5">
                <h2 class="text-uppercase"><span class="d-block">Discover</span> The Collections</h2>
            </div>
            @php
                $i = 0;
                $collections = json_decode(json_encode($data['collections']), true);
            @endphp
            @while ($i < sizeof($collections))
                <div class="row align-items-stretch mt-4">
                    @if (!isset($collections[$i+1]))
                        <div class="col-lg-12">
                            <div class="product-item sm-height full-height bg-gray">
                                @php
                                    $id = $collections[$i]['id'];
                                    $collection = App\Category::find($id);
                                @endphp
                                <a href="#" class="product-category">{{$collections[$i]['name']}} <span>{{$collection->products()->count()}} items</span></a>
                                @if ($collections[$i]['image'])
                                    <img src="{{asset('storage/'.$collections[$i]['image'])}}" alt="Image"
                                         class="img-fluid">
                                @endif
                            </div>
                        </div>
                        @php
                            $i = $i +1;
                        @endphp
                    @else
                        <div class="col-lg-8">
                            <div class="product-item sm-height full-height bg-gray">
                                @php
                                    $id = $collections[$i]['id'];
                                    $collection = App\Category::find($id);
                                @endphp
                                <a href="#" class="product-category">{{$collections[$i]['name']}} <span>{{$collection->products()->count()}} items</span></a>
                                @if ($collections[$i]['image'])
                                    <img src="{{asset('storage/'.$collections[$i]['image'])}}" alt="Image"
                                         class="img-fluid">
                                @endif
                            </div>
                        </div>
                        @php
                            $i = $i +1;
                        @endphp
                        <div class="col-lg-4">
                            @if (isset($collections[$i]))
                                <div class="product-item sm-height bg-gray mb-4">
                                    @php
                                        $id = $collections[$i]['id'];
                                        $collection = App\Category::find($id);
                                    @endphp
                                    <a href="#" class="product-category">{{$collections[$i]['name']}} <span>{{$collection->products()->count()}} items</span></a>
                                    @if ($collections[$i]['image'])
                                        <img src="{{asset('storage/'.$collections[$i]['image'])}}" alt="Image"
                                             class="img-fluid">
                                    @endif
                                </div>
                                @php
                                    $i = $i +1;
                                @endphp
                                @if (isset($collections[$i]))
                                    <div class="product-item sm-height bg-gray">
                                        @php
                                            $id = $collections[$i]['id'];
                                            $collection = App\Category::find($id);
                                        @endphp
                                        <a href="#" class="product-category">{{$collections[$i]['name']}} <span>{{$collection->products()->count()}} items</span></a>
                                        @if ($collections[$i]['image'])
                                            <img src="{{asset('storage/'.$collections[$i]['image'])}}"
                                                 alt="Image" class="img-fluid">
                                        @endif
                                    </div>
                                    @php
                                        $i = $i + 1;
                                    @endphp
                                @endif
                            @endif
                        </div>
                    @endif
                </div>
            @endwhile
        </div>
    </div>
@endsection
