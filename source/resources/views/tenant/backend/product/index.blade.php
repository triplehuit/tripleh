@extends('/tenant/backend/base')

@section('stylesheets')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
@endsection

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <button type="button"
                                class="col-md-12 btn btn-success"
                                data-toggle="modal"
                                data-target="#createProduct">Create New Product</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <button type="button"
                                class="col-md-12 btn btn-success"
                                data-toggle="modal"
                                data-target="#createPromotion">Create New Promotion</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($products as $product)
            <div class="col-md-3">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('storage/'.$product->images()->first()->path) }}" alt="Card image cap">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                                <h5 class="text-sm-center mt-2 mb-1">{{$product->name}}</h5>
                            <div class="location text-sm-center">{{$product->price}}&nbsp;{{ $product->currency()->where('id', $product->currency_id)->first()->symbol }}</div>
                            <div class="location text-sm-center">{!! $product->overview !!}</div>
                        </div>
                        <hr>
                        <div class="weather-category twt-category">
                            <ul>
                                <li class="active">
                                    <h5>{{$product->quantity}}</h5>
                                    items
                                </li>
                                <li>
                                    <h5>{{$product->quantity*$product->price}}</h5>
                                    Values
                                </li>
                                <li>
                                    <h5>3645</h5>
                                    Ordered
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="createProduct" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Create Product</h5>
                </div>
                <div class="modal-body">
                    @if (sizeof($units) <= 0)
                        <p class="text-danger">Please create unit first!</p>
                </div>
                <div class="modal-footer">
                    <a href="{{route('unit.index', ['tenant_id' => $tenant->id])}}" class="btn btn-outline-primary"><i class="fa fa-link"></i>&nbsp;Unit Table</a>
                </div>
                    @else
                    <form id="createProductForm" method="POST" action="{{route('product.store', ['tenant_id' =>  $tenant->id])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="form-control-label">Name</label>
                            <input type="text" name="name" id="name" placeholder="Enter your product name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="slug" class="form-control-label">Slug</label>
                            <input type="text" name="slug" id="slug" placeholder="Enter slug: ao-khoac-den" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="form-control-label">Quantity</label>
                            <input type="text" name="quantity" id="quantity" placeholder="Enter product's quantity" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="unit" class="form-control-label">Unit</label>
                            <select name="unit" id="unit" class="form-control">
                                @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Price" class="form-control-label">Price per product</label>
                            <input type="text" name="price" id="price" placeholder="Enter product's price" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="images">Image</label>
                            <input type="file" id="images" class="form-control-file" multiple name="images[]">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="overview">Overview</label>
                            <textarea id="overview" name="overview"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="description">Description</label>
                            <textarea id="description" name="description"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" form="createProductForm">Confirm</button>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    <script src="{{asset('/assets/backend/product.js')}}"></script>
@endsection
