@extends('/tenant/backend/base')
@section('content')
    @php
    $tenants = Auth::user()->tenants()->get();
    @endphp
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <button type="button"
                                class="col-md-12 btn btn-success"
                                data-toggle="modal"
                                data-target="#createTenant">Create New Shop</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($tenants as $tenant)
            <div class="col-md-3">
                <section class="card">
                    <div class="twt-feed blue-bg">
                        <div class="corner-ribon black-ribon">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <div class="fa fa-twitter wtt-mark"></div>
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{asset('storage/'.$tenant->logo)}}">
                            </a>
                            <div class="media-body">
                                <h2 class="text-white display-6">{{$tenant->name}}</h2>
                                <a href="{{route('page.index', ['slug' => $tenant->slug])}}" class="text-white">View your shop here!</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="weather-category twt-category">
                        <ul>
                            <li class="active">
                                <h5>750</h5>
                                Revenue
                            </li>
                            <li>
                                <h5>865</h5>
                                Orders
                            </li>
                            <li>
                                <h5>3645</h5>
                                Products
                            </li>
                        </ul>
                    </div>
                    <footer class="twt-footer">
                        <p>{{$tenant->description}}</p>
                    </footer>
                </section>
            </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="createTenant" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Create Shop</h5>
                </div>
                <div class="modal-body">
                    <form id="createTenantForm" method="POST" action="{{route('tenant.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="form-control-label">Name</label>
                            <input type="text" name="name" id="name" placeholder="Enter your shop name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="logo">Logo</label>
                            <input type="file" id="logo" class="form-control-file" name="logo">
                        </div>
                        <div class="form-group">
                            <label for="slug" class="form-control-label">Slug</label>
                            <input type="text" name="slug" id="slug" placeholder="slug (Ex: triple-h)" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email" class="form-control-label">Email</label>
                            <input type="text" name="email" id="email" placeholder="Email address" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="address" class="form-control-label">Address</label>
                            <input type="text" name="address" id="address" placeholder="District, City, Country" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="form-control-label">Phone</label>
                            <input type="text" name="phone" id="phone" placeholder="Phone number" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" placeholder="Description" name="description" class="form-control"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" form="createTenantForm">Confirm</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/template/js/main.js')}}"></script>
@endsection
