@extends('/tenant/backend/base')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <button type="button"
                                class="col-md-12 btn btn-success create-category"
                                data-toggle="modal"
                                data-target="#createCategory">Create New Category
                        </button>
                        <input type="hidden" value="{{route('category.store', ['tenant_id' =>  $tenant->id])}}"
                               id="createRoute">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Categories Table</strong>
                    </div>
                    <div class="table-stats order-table ov-h">
                        <table class="table ">
                            <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th>Name</th>
                                <th>Items</th>
                                <th>Values</th>
                                <th>Belongs to</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $index => $category)
                                <tr>
                                    <td class="serial">{{$index+1}}.</td>
                                    <td class="name">{{$category->name}}</td>
                                    <td class="count">{{$category->products()->count()}}</td>
                                    <td class="count">{{$category->values()}}</td>
                                    @if (\App\Category::find($category->parent_id))
                                        <td class="name"><span
                                                class="badge badge-dark">{{\App\Category::find($category->parent_id)->name}}</span>
                                        </td>
                                    @else
                                        <td class="name"><span class="badge badge-light text-dark">Independence</span>
                                        </td>
                                    @endif
                                    <td>
                                        <button class="btn badge badge-warning">Edit</button>
                                        <button class="btn badge badge-danger">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div> <!-- /.table-stats -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    @foreach($categories as $category)
                        <div class="col-md-6">
                            <input type="hidden" data-name="{{$category->name}}"
                                   data-description="{{$category->description}}" id="{{$category->id}}">
                            <div class="card">
                                <img class="card-img-top"
                                     @if($category->image)
                                     src="{{asset('storage/'.$category->image)}}"
                                     @else
                                     src="https://i.imgur.com/ue0AB6J.png"
                                     @endif
                                     alt="Card image cap">
                                <div class="card-body">
                                    <h4 class="card-title mb-3">{{$category->name}}</h4>
                                    <p class="card-text">{{$category->description}}</p>
                                </div>
                                <div class="card-footer">
                                    <button class="config-category" type="button"
                                            class="btn btn-success"
                                            data-toggle="modal"
                                            data-category-id="{{$category->id}}"
                                            data-target="#createCategory"><i class="fa fa-cog"></i>&nbsp;Edit
                                    </button>
                                    <input type="hidden"
                                           value="{{route('category.update', ['tenant_id' => $tenant->id, 'category' => $category->id])}}"
                                           class="update-route-{{$category->id}}">
                                    <button type="button"
                                            class="btn btn-danger delete-category"
                                            data-toggle="modal"
                                            data-category-id="{{$category->id}}"
                                            data-target="#deleteCategory"><i class="fa fa-trash-o"></i>&nbsp;Delete
                                    </button>
                                    <input type="hidden"
                                           value="{{route('category.destroy', ['tenant_id' => $tenant->id, 'category' => $category->id])}}"
                                           class="delete-route-{{$category->id}}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Create Category</h5>
                </div>
                <div class="modal-body">
                    <form id="createCategoryForm" method="POST"
                          action="{{route('category.store', ['tenant_id' =>  $tenant->id])}}"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="form-control-label">Name</label>
                            <input type="text" name="name" id="name" placeholder="Enter your category name"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" id="image" class="form-control-file" name="image">
                        </div>
                        <div class="form-group">
                            <label for="parent" class="form-control-label">Belongs to</label>
                            <select class="selectpicker form-control" id="parent" name="parent[]" multiple data-live-search="Categories">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" name="description" placeholder="Description"
                                      class="form-control"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" form="createCategoryForm">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Category</h5>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete this category?</p>
                    <form id="deleteCategoryForm" method="POST" action="#">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" form="deleteCategoryForm">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="{{asset('assets/backend/category.js')}}"></script>
@endsection
