@section('sidebar')
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{route('tenant.index')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard</a>
                    </li>
                    <li class="menu-title">User Settings</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart"></i>Shops</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-laptop"></i><a href="{{route('tenant.index')}}">Dashboard</a></li>
                        </ul>
                    </li>

                    @php
                        $user = Auth::user();
                        $tenants = $user->tenants()->get();
                    @endphp
                    <li class="menu-title">Shops Management</li>
                    @foreach($tenants as $tenant)
                        <li class="menu-item-has-children dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart"></i>{{$tenant->name}}</a>
                            <ul class="sub-menu children dropdown-menu">
                                <li><i class="menu-icon fa fa-laptop"></i><a href="{{route('tenant.show', $tenant->id)}}">Dashboard</a></li>
                                <li><i class="menu-icon fa fa-sitemap"></i><a href="{{route('category.index', ['tenant_id' => $tenant->id])}}">Categories</a></li>
                                <li><i class="menu-icon fa fa-anchor"></i><a href="{{route('unit.index', ['tenant_id' => $tenant->id])}}">Units</a></li>
                                <li><i class="menu-icon fa fa-umbrella"></i><a href="{{route('product.index', ['tenant_id' => $tenant->id])}}">Products</a></li>
                                <li><i class="menu-icon fa fa-shopping-cart"></i><a href="{{route('order.index', ['tenant_id' => $tenant->id])}}">Orders</a></li>
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
    </nav>
    </aside>
@endsection
