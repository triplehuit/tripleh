@extends('/tenant/backend/base')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <button type="button"
                                class="col-md-12 btn btn-success"
                                data-toggle="modal"
                                data-target="#createUnit">Create New Unit</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Units Table</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-stats order-table ov-h">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="col">#</th>
                                    <th class="col">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($units as $index => $unit)
                                    <tr>
                                        <td class="col">{{$index + 1 }}</td>

                                        <td><span class="col">{{$unit->name}}</span> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- /.table-stats -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createUnit" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Create Unit</h5>
                </div>
                <div class="modal-body">
                    <form id="createUnitForm" method="POST" action="{{route('unit.store', ['tenant_id' =>  $tenant->id])}}">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="form-control-label">Name</label>
                            <input type="text" name="name" id="name" placeholder="Enter your unit name" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" form="createUnitForm">Confirm</button>
                </div>
            </div>
        </div>
    </div>

@endsection
