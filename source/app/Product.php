<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'overview', 'description', 'price', 'tenant_id', 'currency_id', 'quantity', 'views', 'unit_id'];

    /**
     * @return BelongsTo
     */
    public function tenant(){
        return $this->belongsTo('App\Tenant');
    }

    /**
     * @return BelongsTo
     */
    public function currency(){
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return BelongsTo
     */
    public function unit(){
        return $this->belongsTo('App\Unit');
    }

    /**
     * @return BelongsToMany
     */
    public function categories(){
        return $this->belongsToMany('App\Categories','category_products','user_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings(){
        return $this->hasMany('App\Rating');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images(){
        return $this->hasMany('App\Image');
    }
}
