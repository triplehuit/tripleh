<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Tenant extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name','address','phone','email', 'user_id', 'slug', 'description'
    ];


    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * @return HasMany
     */
    public function products(){
        return $this->hasMany('App\Product');
    }

    /**
     * @return HasMany
     */
    public function cms(){
        return $this->hasMany('App\Cms');
    }

    /**
     * @return HasMany
     */
    public function categories(){
        return $this->hasMany('App\Category');
    }

    /**
     * @return BelongsToMany
     */
    public function currencies(){
        return $this->belongsToMany('App\Currency','tenant_currencies', 'tenant_id', 'currency_id');
    }

    /**
     * @return HasMany
     */
    public function units(){
        return $this->hasMany('App\Unit');
    }

    /**
     * @return BelongsToMany
     */
    public function components(){
        return $this->belongsToMany('App\Component','tenant_components', 'tenant_id','component_id');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }
}
