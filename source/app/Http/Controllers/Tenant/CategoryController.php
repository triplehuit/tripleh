<?php

namespace App\Http\Controllers\Tenant;

use App\Category;
use App\Repositories\Eloquents\CategoryRepository;
use App\Repositories\Eloquents\TenantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    protected $tenantRepository;
    protected $categoryRepository;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository, TenantRepository $tenantRepository)
    {
        $this->tenantRepository = $tenantRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tenant = $this->tenantRepository->find($request->tenant_id);
        $categories = $tenant->categories()->get();
        $title = 'Category';
        return view('/tenant/backend/category/index')->with('categories', $categories)->with('title', $title)->with('tenant', $tenant);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        if ($request->method() === 'POST'){
            $data = array(
                'name' => $request->name,
                'tenant_id' => $request->tenant_id,
                'description' => $request->description,
                'parents_id' => ($request->parent) ? $request->parent : null,
                'image' => $request->file('image')
            );
            if ($this->categoryRepository->create($data)){
                return redirect()->route('category.index', ['tenant_id' => $request->tenant_id]);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $data = array(
            'name' => $request->name,
            'tenant_id' => $request->tenant_id,
            'description' => $request->description,
            'image' => $request->file('image')
        );
        if ($this->categoryRepository->update($request->category, $data)){
            return redirect()->route('category.index', ['tenant_id' => $request->tenant_id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        if ($this->categoryRepository->delete($request->category)){
            return redirect()->route('category.index', ['tenant_id' => $request->tenant_id]);
        }
    }
}
