<?php

namespace App\Http\Controllers\Tenant;

use App\Currency;
use App\Image;
use App\Product;
use App\Repositories\Eloquents\ProductRepository;
use App\Repositories\Eloquents\TenantRepository;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    protected $productRepository;
    protected $tenantRepository;

    /**
     * ProductController constructor.
     * @param ProductRepository $productRepository
     * @param TenantRepository $tenantRepository
     */
    public function __construct(ProductRepository $productRepository, TenantRepository $tenantRepository)
    {
        $this->productRepository = $productRepository;
        $this->tenantRepository = $tenantRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $units = Unit::get();
        $tenant = Auth::user()->tenants()->where('id', $request->tenant_id)->first();
        $products = $this->productRepository->all($tenant);
        $categories = $tenant->categories()->get();
        return view('/tenant/backend/product/index')
            ->with('products', $products)
            ->with('tenant', $tenant)
            ->with('units', $units)
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $tenant = Auth::user()->tenants()->where('id', $request->tenant_id)->first();
        $product = new Product(array(
            'name' => $request->name,
            'slug' => $request->slug,
            'overview' => $request->overview,
            'description' => $request->description,
            'quantity' => $request->quantity,
            'currency_id' => 1,
            'price' => $request->price,
            'tenant_id' => $tenant->id,
            'views' => 0,
            'unit_id' =>$request->unit
        ));
        $product->save();
        if ($request->file('images')){
            foreach($request->file('images') as $file){
                $image = new Image();
                $product->images()->save($image);
                $image->path = $file->storeAs('public/tenant/'.''.$tenant->id.''.'/product/'.''.$product->slug,'image-'.''.$image->id);
                $image->path = str_replace('public/','',$image->path);
                $product->images()->save($image);
            }
        }
        redirect()->route('product.index', ['tenant->id' => $tenant->id]);
    }

}
