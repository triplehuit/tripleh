<?php

namespace App\Http\Controllers\Tenant;

use App\Repositories\EloquentRepository;
use App\Repositories\Eloquents\TenantRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TenantController extends Controller
{
    /**
     * @var TenantRepositoryInterface|EloquentRepository
     */
    protected $tenantRepository;

    /**
     * TenantController constructor.
     * @param TenantRepository $tenantRepository
     */
    public function __construct(TenantRepository $tenantRepository)
    {
        $this->middleware('auth');
        $this->tenantRepository = $tenantRepository;
    }

    /**
     * @return Factory|View
     */
    public function index(){
        $tenants = $this->tenantRepository->get(null, Auth::user());
        return view('/tenant/backend/user/index')->with('tenants', $tenants);
    }

    public function showTenant(Request $request){
        $tenant = $this->tenantRepository->find($request->id);
        return view('/tenant/backend/show')->with('tenant', $tenant);
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function storeTenant(Request $request){
        if ($request->method() === 'POST'){
            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'address' => $request->address,
                'phone' => $request->phone,
                'user_id' => Auth::user()->id,
                'slug' => $request->slug,
                'description' => $request->description,
                'logo' => $request->file('logo')
            );
            if ($this->tenantRepository->create($data)){
                return redirect(route('tenant.index'));
            } else {
                // RESPONSE ERRORS
            }
        } else {
            return $this->index();
        }
    }
}
