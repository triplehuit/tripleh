<?php

namespace App\Http\Controllers\Tenant;

use App\Category;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquents\CategoryRepository;
use App\Repositories\Eloquents\ProductRepository;
use App\Repositories\Eloquents\TenantRepository;
use App\Tenant;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PageController extends Controller
{
    /**
     * @var TenantRepository
     */
    protected $tenantRepository;
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * PageController constructor.
     * @param TenantRepository $tenantRepository
     * @param CategoryRepository $categoryRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(TenantRepository $tenantRepository, CategoryRepository $categoryRepository, ProductRepository $productRepository)
    {
        $this->tenantRepository = $tenantRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request){
        $tenant = $this->tenantRepository->get($request->slug, null);
        $slug = $request->slug;
        $data = array(
            # Collections
            'collections' => $this->categoryRepository->getCategoryNoParent($tenant->id),
            # Popular items
            'popular_items' => $this->productRepository->popular($tenant, null, 6),
        );
        return view('/tenant/frontend/index/index')->with('tenant', $tenant)->with('data', $data)->with('slug', $slug);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function shop(Request $request){
        $tenant = Tenant::where('slug', $request->slug)->first();
        $product = $tenant->products()->where('slug', $request->productSlug)->first();
        return view('/tenant/frontend/product/shop')->with('tenant', $tenant)->with('product', $product);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function shopSingle(Request $request){
        $tenant = Tenant::where('slug', $request->slug)->first();
        $slug = $request->slug;
        $product = $this->productRepository->findBySlug($request->product_slug, $tenant);
        $data = array(
            'collections' => $this->categoryRepository->getCategoryNoParent($tenant->id),
            'product' => $product
        );
        return view('/tenant/frontend/product/detail')->with('tenant', $tenant)->with('data', $data)->with('slug', $slug);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function category(Request $request){
        $tenant = Tenant::where('slug', $request->slug)->first();
        $category = Category::where('id', $request->id)->first();
        $products= $category->products()->get();
        return view('/tenant/frontend/product/shop')
            ->with('tenant', $tenant)
            ->with('products', $products)
            ->with('tenant', $tenant);
    }

    public function addToCart(Request $request){
        $tenant = Tenant::where('slug', $request->slug)->first();
        $product = $this->productRepository->findBySlug($request->slug, $tenant);
        # TODO: Add to cart
        # TODO: Flash message
    }
}
