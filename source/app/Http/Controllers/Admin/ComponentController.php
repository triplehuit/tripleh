<?php

namespace App\Http\Controllers\Admin;

use App\Component;
use App\CType;
use App\Http\Controllers\Controller;
use App\Script;
use App\Style;
use Illuminate\Http\Request;

class ComponentController extends Controller
{
    public function index(){
        $components = Component::get();
        $data = array(
            'components' => $components,
            'title' => 'List Components'
        );
        return view('/admin/backend/component/index')->with('data', $data);
    }
    public function create(){
        $types = CType::get();
        $data = array(
            'types' => $types,
            'title' => 'Create Component'
        );
        return view('/admin/backend/component/create')->with('data', $data);
    }

    public function store(Request $request){
        // Check parameter
        if ($request->file('component') === null){
            return redirect()->route('component.create');
        }
        if ($request->file('component')->getClientOriginalExtension() != 'php'){
            return redirect()->route('component.create');
        }


        // Init
        $js = new Script();
        $style = new Style();
        $component = new Component();
        $js->save();
        $style->save();
        $component->name = $request->name;
        $component->type_id = $request->type;
        $component->save();

        // Setting parameter && storage files.
        if ($request->file('js') !== null && $request->file('js')->getClientOriginalExtension() === 'js'){
            $js->path = $request->file('js')->storeAs('components', 'js-' .''. $component->id.''.'.js');
        }

        if ($request->file('css') !== null && $request->file('css')->getClientOriginalExtension() === 'css'){
            $style->path = $request->file('css')->storeAs('components', 'css-' .''. $component->id.''.'.css');
        }
        $component->path = $request->file('component')->storeAs('components', 'component-' .''. $component->id.''.'.blade.php');
        $component->style_id = $style->id;
        $component->script_id = $js->id;

        // Storage
        $component->save();
        $js->save();
        $style->save();
        return redirect()->route('component.index');
    }
}
