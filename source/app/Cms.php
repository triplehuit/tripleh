<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Cms extends Model
{
    /**
     * @return BelongsTo
     */
    public function tenant(){
        return $this->belongsTo('tenants');
    }
}
