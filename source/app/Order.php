<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['code', 'tenant_id', 'status_id'];

    public function tenant(){
        return $this->belongsTo('App\Tenant');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
