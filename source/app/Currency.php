<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Currency extends Model
{
    /**
     * @return HasMany
     */
    public function products(){
        return $this->hasMany('App\Product');
    }

    /**
     * @return BelongsToMany
     */
    public function tenants(){
        return $this->belongsToMany('App\Tenant','tenant_currencies', 'currency_id','tenant_id');
    }
}
