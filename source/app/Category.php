<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{

    protected $fillable = ['name','tenant_id', 'description'];

    /**
     * @return BelongsTo
     */
    public function tenant(){
        return $this->belongsTo('App\Tenant');
    }

    /**
     * @return BelongsToMany
     */
    public function parents(){
        return $this->belongsToMany('App\Category', 'category_categories', 'child_id', 'parent_id');
    }

    /**
     * @return BelongsToMany
     */
    public function children(){
        return $this->belongsToMany('App\Category', 'category_categories', 'parent_id', 'child_id');
    }

    /**
     * @return BelongsToMany
     */
    public function products(){
        return $this->belongsToMany('App\Product','category_products','category_id', 'product_id');
    }

    /**
     * @return float|int
     */
    public function values(){
        $values = 0;
        $products = $this->products()->get();
        foreach($products as $product){
            $values = $values + $product->quantity * $product->price;
        }
        return $values;
    }
}
