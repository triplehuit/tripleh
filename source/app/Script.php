<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Script extends Model
{
    public function component(){
        return $this->belongsTo('App\Component');
    }
}
