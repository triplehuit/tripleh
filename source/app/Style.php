<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    public function component(){
        return $this->belongsTo('component');
    }
}
