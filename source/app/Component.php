<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    public function type(){
        return $this->hasOne('App\Type');
    }

    public function style(){
        return $this->hasOne('App\Style');
    }

    public function script(){
        return $this->hasOne('App\Script');
    }

    public function tenants(){
        return $this->belongsToMany('App\tenant', 'tenant_components','component_id', 'tenant_id');
    }
}
