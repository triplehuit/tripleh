<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CType extends Model
{
    public function components(){
        return $this->belongsTo('App\Component');
    }
}
