<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class EloquentRepository implements RepositoryInterface {
    /**
     * @var Model
     */
    protected $_model;

    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * Get Model
     * @return string
     */
    abstract public function getModel();

    /**
     * @throws BindingResolutionException
     * Set Model
     */
    public function setModel(){
        $this->_model = App()->make(
            $this->getModel()
        );
    }

    /**
     * @return Collection|Model[]
     */
    public function getAll(){
        return $this->_model->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id){
        return $this->_model->find($id);
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes){
        return $this->_model->create($attributes);
    }

    /**
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function update($id, array $attributes){
        $result = $this->find($id);
        if ($result){
            $result->update($attributes);
            return $result;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id){
        $result = $this->find($id);
        if ($result){
            $result->delete();
            return true;
        }
        return false;
    }
}
