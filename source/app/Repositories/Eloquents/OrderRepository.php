<?php

namespace App\Repositories\Eloquents;

use App\Order;
use App\Repositories\EloquentRepository;

class OrderRepository extends EloquentRepository implements OrderInterface
{

    /**
     * Get Model
     * @return string
     */
    public function getModel()
    {
        return Order::class;
    }

    public function setOrder()
    {

    }
}
