<?php

namespace App\Repositories\Eloquents;

use App\Product;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\EloquentRepository;

class ProductRepository extends EloquentRepository implements ProductInterface {

    /**
     * @return string
     */
    public function getModel(){
        return Product::class;
    }

    /**
     * @param null $tenant
     * @return Product[]|Collection
     */
    public function all($tenant = null){
        if ($tenant){
            return Product::all();
        } else
        return $tenant->products()->get();
    }

    /**
     * @param $id
     * @param null $tenant
     * @return mixed
     */
    public function find($id, $tenant = null){
        if (!$tenant){
            return Product::find($id)->first();
        } else{
            return $tenant->products()->find($id)->first();
        }
    }

    /**
     * @param null $tenant Tenant Model, default = null
     * @param null $user User Model, default = null
     * @param int $size size array, default = 10
     * @return mixed
     */
    public function popular($tenant = null, $user = null, $size = 10){
        if ($tenant){
            return $tenant->products()->orderBy('views')->take($size)->get();
        }
    }

    public function findBySlug($slug, $tenant = null){
        if (!$tenant){
            return Product::where('slug', $slug)->first();
        } else {
            return $tenant->products()->where('slug', $slug)->first();
        }
    }
}
