<?php

namespace App\Repositories\Eloquents;

use App\Tenant;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Repositories\EloquentRepository;

class TenantRepository extends EloquentRepository implements TenantInterface
{

    /**
     * @return string
     */
    public function getModel()
    {
        return Tenant::class;
    }

    /**
     * @param null $slug
     * @param null $user
     * @return bool
     */
    public function get($slug = null, $user = null)
    {
        if ($slug) {
            if ($user) {
                return $user->tenants()->where('slug', $slug)->first();
            } else
                return Tenant::where('slug', $slug)->first();
        }
        if ($user) {
            return $user->tenants()->get();
        }
        return false;
    }

    /**
     * @param $data
     * @return Exception|bool|Exception
     */
    public function create($data)
    {
        $tenant = new Tenant(array(
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'phone' => $data['phone'],
            'user_id' => $data['user_id'],
            'slug' => $data['slug'],
            'description' => $data['description']
        ));
        try {
            Auth::user()->tenants()->save($tenant);
            $logoExtension = $data['logo']->getClientOriginalExtension();
            $logoPath = $data['logo']->storeAs('public/tenant/' . '' . $tenant->id, 'logo.' . '' . $logoExtension);
            $logoPath = str_replace('public/', '', $logoPath);
            $tenant->logo = $logoPath;
            Auth::user()->tenants()->save($tenant);
            return true;
        } catch (Exception $e) {
            return $e;
        }
        return false;
    }
}
