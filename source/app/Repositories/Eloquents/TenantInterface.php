<?php

namespace App\Repositories\Eloquents;

interface TenantInterface{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);
}
