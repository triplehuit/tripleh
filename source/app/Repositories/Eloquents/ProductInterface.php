<?php

namespace App\Repositories\Eloquents;

interface ProductInterface{
    public function popular($tenant = null, $user = null, $size = 10);
}
