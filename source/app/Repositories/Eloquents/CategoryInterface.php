<?php

namespace App\Repositories\Eloquents;

interface CategoryInterface{
    /**
     * @return mixed
     */
    public function getCategoryNoParent($tenant_id);
}
