<?php

namespace App\Repositories\Eloquents;

use App\Category;
use App\Repositories\EloquentRepository;
use App\Tenant;
use mysql_xdevapi\Exception;

class CategoryRepository extends EloquentRepository implements CategoryInterface
{
    /**
     * @return string
     */
    public function getModel(){
        return Category::class;
    }


    /**
     * @param $data
     * @return Category
     */
    public function create($data){
        $category = new Category(array(
            'name' => $data['name'],
            'tenant_id' => $data['tenant_id'],
            'description' => $data['description'],
        ));
        $category->save();

        if ($data['image']){
            $imageExtension = $data['image']->getClientOriginalExtension();
            $imagePath = $data['image']->storeAs('public/tenant/' . '' . $data['tenant_id'].'/category/'.$category->id, 'image.'.$imageExtension);
            $imagePath = str_replace('public/', '', $imagePath);
            $category->image = $imagePath;
            $category->save();
        }

        if ($data['parents_id']){
            $category->parents()->attach($data['parents_id']);
        }

        return $category;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool|mixed
     */
    public function update($id, $data){
        $category = Category::find($id);
        if (!$category) return false;

        $category->name = $data['name'];
        $category->tenant_id = $data['tenant_id'];
        $category->description = $data['description'];

        if ($data['image']){
            $imageExtension = $data['image']->getClientOriginalExtension();
            $imagePath = $data['image']->storeAs('public/tenant/' . '' . $data['tenant_id'].'/category/'.$category->id, 'image.'.$imageExtension);
            $imagePath = str_replace('public/', '', $imagePath);
            $category->image = $imagePath;
        }

        $parents_id = $data['parents'];
        $category->parents()->sync($parents_id);
    }

    /**
     * @return mixed
     */
    public function getCategoryNoParent($tenant_id){
        return Tenant::find($tenant_id)->categories()->doesnthave('parents')->get();
    }
}
