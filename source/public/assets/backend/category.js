$('document').ready(function () {
    initEvent();
});

function initEvent() {
    $('.selectpicker').selectpicker();
    $('.create-category').on('click', function () {
        $('#createCategoryForm').attr('action', $('#createRoute').val());
        if ($('.form-method')) {
            $('.form-method').val('POST');
        }
    });
    $('.config-category').on('click', function () {
        let id = $(this).data('category-id');
        $('#name').val($('#' + id).data('name'));
        $('#description').val($('#' + id).data('description'));
        $('#createCategoryForm').attr('action', $('.update-route-' + id).val());
        let inputMethodPut = '<input type="hidden" name="_method" value="PUT" class="form-method">';
        $('#createCategoryForm').append(inputMethodPut);
    });

    $('.delete-category').on('click', function () {
        let id = $(this).data('category-id');
        $('#deleteCategoryForm').attr('action', $('.delete-route-' + id).val());
    });
}

