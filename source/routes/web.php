<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['middleware' => 'auth'], function(){
    # TENANT
    Route::group(['prefix' => 'tenant', 'namespace' => 'Tenant'] ,function(){
        Route::get('/', 'TenantController@index')->name('tenant.index');
        Route::post('/', 'TenantController@storeTenant')->name('tenant.store');
        Route::get('/{id}', 'TenantController@showTenant')->name('tenant.show');
        Route::post('/setActive', 'TenantController@setActive')->name('tenant.setActive');
        Route::get('/create','TenantController@createTenant')->name('tenant.create');
        Route::get('/list', 'TenantController@listTenant')->name('tenant.list');
    });

    # TENANT'S RESOURCES
    Route::group(['prefix'=> 'tenant/{tenant_id}', 'namespace' => 'Tenant'], function(){
        Route::resource('/product', 'ProductController');
        Route::resource('/category', 'CategoryController');
        Route::resource('/unit','UnitController');
        Route::resource('/order', 'OrderController');
    });

    # ADMINISTRATOR
    Route::group(['middleware' => 'superadmin'], function(){
        Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
            Route::get('/', 'HomeController@index')->name('admin.home');
            Route::group(['prefix' =>'component'], function(){
                Route::get('/index', 'ComponentController@index')->name('component.index');
                Route::get('/create', 'ComponentController@create')->name('component.create');
                Route::post('/create', 'ComponentController@store')->name('component.store');
            });
        });
    });
    # NORMAL USER
});

# GUEST
Route::group(['prefix' => 'shop', 'namespace' =>'Tenant'], function(){
    Route::get('/{slug}' , 'PageController@index')->name('page.index');
    Route::get('/{slug}/product', 'PageController@shop')->name('page.shop');
    Route::get('/{slug}/product/{product_slug}', 'PageController@shopSingle')->name('page.shopSingle');
    Route::post('/{slug}/product{product_slug}', 'PageController@addToCart')->name('page.addToCart');
    Route::get('/{slug}/category/{id}', 'PageController@category')->name('page.category');
});

