<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_currencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tenant_id')->unsigned();
            $table->bigInteger('currency_id')->unsigned();
            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_currencies', function(Blueprint $table){
           $table->dropForeign(['tenant_id']);
           $table->dropForeign(['currency_id']);
        });
        Schema::dropIfExists('tenant_currencies');
    }
}
