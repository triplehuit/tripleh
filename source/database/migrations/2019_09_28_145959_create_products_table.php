<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->string('overview');
            $table->string('description');
            $table->double('quantity');
            $table->double('price');
            $table->bigInteger('views')->default(0);
            $table->bigInteger('currency_id')->unsigned();
            $table->bigInteger('unit_id')->unsigned();
            $table->bigInteger('tenant_id')->unsigned();
            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('unit_id')->references('id')->on('units');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table){
            $table->dropForeign(['currency_id']);
            $table->dropForeign(['tenant_id']);
            $table->dropForeign(['unit_id']);
        });
        Schema::dropIfExists('products');
    }
}
