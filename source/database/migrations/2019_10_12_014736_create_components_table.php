<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('type_id')->unsigned();
            $table->string('path')->nullable();
            $table->bigInteger('style_id')->unsigned()->nullable();
            $table->bigInteger('script_id')->unsigned()->nullable();
            $table->foreign('style_id')->references('id')->on('styles');
            $table->foreign('script_id')->references('id')->on('scripts');
            $table->foreign('type_id')->references('id')->on('c_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('components', function(Blueprint $table){
           $table->dropForeign(['style_id']);
           $table->dropForeign(['script_id']);
           $table->dropForeign(['type_id']);
        });
        Schema::dropIfExists('components');
    }
}
