<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tenant_id')->unsigned();
            $table->bigInteger('component_id')->unsigned();
            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('component_id')->references('id')->on('components');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_components', function(Blueprint $table){
           $table->dropForeign(['tenant_id']);
           $table->dropForeign(['component_id']);
        });
        Schema::dropIfExists('tenant_components');
    }
}
