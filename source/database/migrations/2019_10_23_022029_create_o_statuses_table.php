<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status');
            $table->timestamps();
        });
        DB::table('o_statuses')->insert(
            array(
                array(
                    'id' => 1,
                    'status' => 'done',
                ),
                array(
                    'id' => 2,
                    'status' => 'transporting'
                ),
                array(
                    'id' => 3,
                    'status' => 'waiting'
                ),
                array(
                    'id' => 4,
                    'status' => 'draft'
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_statuses');
    }
}
